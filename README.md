# README #

Ansible playbook to install Home Assistant Supervised on a Debian virtual machine

The playbook is a step to step conversion of the original shell script form:
https://github.com/home-assistant/supervised-installer


### Prerequisites ###

* Debian host, fully updated (tested only on VmWare Player with Debian 10 host)
* The user that run the playbook must have access to the *new* hassio host using a ssh key
* The user must be member of the SUDO group


### Configuration ###

Add the *new* hassio host ip address in the Ansible hosts file

Configure ssh access with: ssh-copy-id -i /home/****ANSIBLE_USER****/.ssh/id_rsa.pub ****REMOTE_USER****@****REMOTE_IP_ADDRESS****


### Running the playbook ###

Use the command:

$ansible-playbook pb_deb_hassio.yml -K

Insert the remote user password to permit sudo